# videoserver

Video server - basic (fixed room, insecure)

## Design considerations

Persistently identifying camera views is important so that camera feeds can be correctly routed, even after the browser is restarted. It is not practical to manually identifiy each camera feed after a browser restart - that could happen at any time. 

Typically, this process is complicated by at least two things
a/ many camera views look alike when there are class sets of identical equipment in identical settings, and it is easy to confuse which machine you are accessing, if using rdp (does silly things to microphone settings on windows) or kvm (not always correctly identifying the machine you are on).
b/ camera serial numbers are denied to browsers to avoid fingerprinting users across sessions

Going in reverse order:

No serial numbers available in browser: Google allows an obfuscated camera ID to persist across reboots, until the user (that's us) does a refresh of browsing data for privacy reasons. Therefore, so long as we don't clear our browsing history, associating that camera ID with a view ID in the cache is sufficient for operation over many months. If the camera is replaced, then the replacement camera needs to be identified. The pain of a native webRTC implementation outweighs the gain of information about a serial number.

Identical camera views: it is usually necessary to temporarily salt the camera view with an identifying feature, such as a location label, which can be seen long enough to identify the view to the videoserver app on the experiment. This process can be automated by using bar codes or QR codes, although currently it does not seem practical to read QR codes or bar codes that are small, and off to the sides of the image (admittedly that's because many QR code readers crop to the middle of the image, but distortions are larger at the edges of the view from a wide angle lens), so it is not currently sufficient to place a small QR code in the scene forever after.


Initial solution:
Manually enter a view identifier into the browser running the video server.

Medium term solution:
If experiments do not have outgoing 443 access, then implement automatic reading of a 1D or 2D code

Medium term solution:
If experiment has outgoing 443 access, then select camera from a pre-configured list of known experiments. User will still need to pop some sort of identifier into the view to help them get the right views.

Longer term solutions:
Automate various aspects of the selection and verification of the camera views.
For example, supply sample images from each of the multiple camera views of an experiment, and have the system automatically determine which images correspond to what view. Does not help with the identification of the location. This could help identify when a camera has become dislodged, etc.


### Codes

One approach to identifying a camera is to show it a bar code or qr code. This can help when many similar camera views are being seen remotely. However, if you have access to the experiment, and the experimental PC, then it is presumably practical to place a specific object or name card in the view, then enter an appropriate identification code manually.

## Bar codes

`Openvidu` implements the `zbar` filter from `gstreamer`.

[Zbar](http://zbar.sourceforge.net/about.html) supports 
* EAN13
* UPC-A
* UPC-E
* EAN8
* code 128
* code39

It will later support PDF417 and EAN/UPC.

Apparently code 39 is good for reading at distance, due to the code structure.
Checking can be added by adding a digit modulo 43.

### QR codes

Kurento media server has a qr code reader - it seems. Performance would likely to be better if a QR code reader was implemented client side - it would save adding a whole new class of video sessions that are orphaned without identification - a possible security issue and DoS route.


## Setup Procedure

Authorisation matters for serving when media relay is involved, because of the cost of providing the media relay. Therefore, the authorisation to serve video should be managed according to time and quantity. Thus a possible token flow would be:-

Laboratory owner requests a set of tokens of finite durations, e.g. 5 experiments of 2 camera views each for 3 months from 15th March to 15th June.
Media relay owner grants time-limited tokens to laboratory manager to use the video relay, e.g. 10 tokens for 14 March 23:59:59 to 16 June 00:00:01
Laboratory manager assigns those tokens to the experiments.
They could be cached in the browser either by manual cut and paste into text box (think ssh tokens in gitlab), or by starting the browser with a script that reads tokens from file and appends to url as url parameters. 
Lab manager associates each camera with a view identification, and a token to stream.
Browser is restarted and lab manager checks that the videos are streaming as required, by starting a video check client that is aware of all of the ids assigned in the lab (note this implies an overall map of all the experiments that the lab manager is responsible for).
Video server app obtains video stream from each camera, and publishes it to the appropriate sessionon the media relay server, using the authentication tokens and identification tokens details as a gatekeeper.

The gatekeeper checks to see whether anyone else is using those identification tokens, and if they are, it needs to either refuse the newcomer, or get rid of the old. Letting the latest supplier have priority is probably a defence against stale/hanging sessions that have occurred on the client side. If the experiment restarts and authenticates to server, then server can presume old client is dead. This would allow someone to take over a video feed _if_ they have the appropriate auth, but this could happen in any order (agressor establishes stream, then genuine supplier, or vice versa) so there is no additional vulnerability in choosing to favour the most recent supplier.


Issues 

Camera view identification requires signing by an authority, so that a token cannot be 'borrowed' from an experiment and used for a different experiment without express permission of the lab manager.

Revocation - nor currently considered necessary to implement immediately, as it requires keeping a deny list - but so long as only finite length tokens, the deny list need only be as long as the list of current tokens that have been denied. so quite doable.

Token update is out-of-band / If token update is done manually then this will be painful if tokens are shorter than the installation lifetime of the experiments, which is quite probably the case. Probably better that more sophisticated / long term install experiments have a means by which a file with tokens in it can be updated remotely. This should be managed out-of-band and not part of the video server setup, because it will over complicate the setup of video in the early stages of setting up a new facility.

Token is out of date - a notification to the lab manager is needed- but again this is an out-of-band communication that is outside the purview of the video system itself.


So .... 
Media server authorises sessions, and expects its own jwt tokens back one per view
Lab manager authorises view naming, and expects media server to only host authorised views
Each publisher wanting to publish needs to send in a token from each authority.
It'd be a security issue if each lab manager had to give in their secret to the media server relay, so each lab manager therefore needs to provide an interface against which the token can be checked. This could be as simple as a service that takes their secret and authenticates the jwt token, that can be setup on our own server under an account only they have access too, or a server that they configure and run as part of their lab management system.

Media server needs a layer that can authenticate requests, and end streams that are no longer needed.

If the media server is the gateway to the streams, then presumably an experiment can just connect once, to the server, and the authentication layer can take care of ending subscriber feeds via a server disconnect. This implies that the media server is a centralised broker for the particular experiments that it is serving. How will it recover from a restart? No media server, no experiments. Implies a fleet of media servers that can step in for their bretheren if one falls over, with session state being managed by the authentication layer.



If one is buying tokens, then presumably one can be expected to want to pay less for less demanding video, so do we have small, medium and large size video feed prices based on resolution?

What about resolution control and pausing? Can the client simply cut a feed they don't want? How long to get it back? Can they ask for transcoding? Should the server side send out small, medium and large feeds anyway? So that there is no transcoding....

users should be encouraged to supply a good experience, so as to support the growth of non-traditional practical work. Therefore, it makes sense to assume the lowest cost option is the most popular choice, e.g. offer s,m,l,xl feeds from server and allow user to get the one that suits their bandwidth best.

This implies the view name may need some convention, e.g. <name>_<size>

front.s
front.m
front.l
front.xl

sizes: [s,m,l,xl]
views: [primary, secondary, tertiary, front, back, left, right, top, bottom, wide, zoom, normal, alternative, <freetext>]


Ending a session

If a user cannot connect to a session that they should not connect to, then all is good.

If we control who can have access to a session by generating a token, and then control disconnecting them, then we don't need to continually log the server in and out. Unless it's browser crashes, in which case we need to ensure it is only sending one copy of the feed!

Fallback - if a disconnected user is somehow able to re-use their token, then we need to 


Video checkers - could sit anywhere on the net, for obvious reasons, it at least needs to be stunnable/turnable, but people could earn credits by running a videochecker app. Can we do it so they don't see the experiments?! I.e. opening and closing their browsers will be annoying .... If ICE is slow for a particular video checker, then we would want to only use it in extremis. And it implies that video checking is not something to assume we can do in a few seconds.

Due to filehandling in browsers, long sessions are also probably good. But, we need to figure out how long to push that. Be nice not to do a reboot every night a-la windows. It is assumed linux boxes will host experiments. because there is no need to host elvis II in new installations - and anyone hosting ELVIS II can provide a linux machine for the video side, and data side, which speaks to the windows machine running the LabVIEW code, so no worries there. ELVIS III does not need windows machine - or indeed any machine for the data side (just a machine to handle video).


 

## Prototyping

Server end

Provide a way to start a publisher that takes the default camera, names the view, and puts it in a session, and keeps trying until that is done.

Refinement - handle multiple cameras, and associate a particular viewID with each camera


Client end:
reject all but specified views, and assign views to specific dom IDs

refinement:
show one view at a time with a drop down to change views / or icon 



